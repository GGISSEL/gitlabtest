import java.io.FileNotFoundException;
import java.util.Scanner;

public class Frontend implements FrontendInterface {

    Scanner scanner;
    Backend backend;
    boolean dataLoaded = false;

    public Frontend(Backend backend, Scanner scanner){
        this.backend = backend;
        this.scanner = scanner;
    }

    public void mainMenu() {
        boolean looping = true;
        boolean inputValid;

        System.out.println("Welcome to the Flight Router");
        System.out.println("============================");
        System.out.println("Select an option: ");
        System.out.println("[L] Load File\n[S] Show Stats\n[R] Show Shortest Route\n[E] Exit");
        String input = scanner.nextLine();
        while (looping) {
            if (input.equalsIgnoreCase("L")) {
                inputValid = true;
                input = "";
                this.loadFile();
                dataLoaded = true;
            }
            else if (input.equalsIgnoreCase("S")) {
                inputValid = true;
                if (dataLoaded) {
                    this.showStats();
                    input = "";
                }
                else {
                    System.out.println("Please load file data");
                    input = "";
                    continue;
                }

            }
            else if (input.equalsIgnoreCase("R")) {
                inputValid = true;
                if (dataLoaded) {
                    this.shortestRoute();
                    input = "";
                }
                else {
                    System.out.println("Please load file data");
                    input = "";
                    continue;
                }
            }
            else if (input.equalsIgnoreCase("E")) {
                inputValid = true;
                exit();
                looping = false;
            }
            else if (input.equals("")) {
                System.out.println("\nPlease choose new command or exit.");
                System.out.println("[L] Load File\n[S] Show Stats\n[R] Show Shortest Route\n[E] Exit");
                input = scanner.nextLine();
                inputValid = true;
            }
            else
                inputValid = false;

            while (!inputValid) {
                System.out.println("Input invalid, try again.");
                String newInput = scanner.nextLine();
                if (newInput.equalsIgnoreCase("L") ||
                        newInput.equalsIgnoreCase("S") ||
                        newInput.equalsIgnoreCase("R") ||
                        newInput.equalsIgnoreCase("E")) {
                    inputValid = true;
                    input = newInput;
                }
            }


        }
    }

    @Override
    public boolean loadFile() {
        System.out.println("Enter Filename to Load:");
        String fileName = scanner.nextLine();
        while (true){
            try {
                this.backend.readDataFromFile(fileName);
                System.out.println("File successfully loaded.");
                return true;
            } catch (RuntimeException | FileNotFoundException e) {
                System.out.println("Invalid Filename, try again:");
            }
            fileName = scanner.nextLine();
        }
    }


    public void showStats() {
        System.out.println("These are the statistics:");
        System.out.println(this.backend.getSummary());
    }


    public void shortestRoute() {
        System.out.println("Please input starting airport:");
        String startAirport = scanner.nextLine();
        System.out.println("Please input destination airport");
        String destinationAirport = scanner.nextLine();

        ShortestPathInterface shortestPath = this.backend.findShortestPath(startAirport, destinationAirport);

        System.out.println();
        System.out.println("Shortest Route: " + shortestPath.getRoute());
        System.out.println("Miles Between Airports: " + shortestPath.findMilesPerSegment());
        System.out.println("Total Miles: " + shortestPath.getTotalMiles());
    }


    public void exit() {
        System.out.println("\nApplication exited.");
    }


    public static void main(String[] args){
        Backend backend = new Backend(new DijkstraGraph<String, Integer>(new PlaceholderMap<>()));
        Scanner scan = new Scanner(System.in);
        Frontend frontend = new Frontend(backend, scan);
        frontend.mainMenu();
        scan.close();
    }
}
