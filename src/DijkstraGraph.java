// --== CS400 File Header Information ==--
// Name: George Gissel
// Email: ggissel@wisc.edu
// Group and Team: G13 2
// Group TA: Robert Nagel
// Lecturer: Florian Heimerl
// Notes to Grader: <optional extra notes>

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.*;

import java.util.PriorityQueue;
import java.util.*;

/**
 * This class extends the BaseGraph data structure with additional methods for computing the total
 * cost and list of node data along the shortest path connecting a provided starting to ending
 * nodes. This class makes use of Dijkstra's shortest path algorithm.
 */

public class DijkstraGraph<NodeType, EdgeType extends Number> extends BaseGraph<NodeType, EdgeType>
    implements GraphADT<NodeType, EdgeType> {

  /**
   * While searching for the shortest path between two nodes, a SearchNode contains data about one
   * specific path between the start node and another node in the graph. The final node in this path
   * is stored in its node field. The total cost of this path is stored in its cost field. And the
   * predecessor SearchNode within this path is referened by the predecessor field (this field is
   * null within the SearchNode containing the starting node in its node field).
   *
   * SearchNodes are Comparable and are sorted by cost so that the lowest cost SearchNode has the
   * highest priority within a java.util.PriorityQueue.
   */
  protected class SearchNode implements Comparable<SearchNode> {
    public Node node;
    public double cost;
    public SearchNode predecessor;

    public SearchNode(Node node, double cost, SearchNode predecessor) {
      this.node = node;
      this.cost = cost;
      this.predecessor = predecessor;
    }

    public int compareTo(SearchNode other) {
      if (cost > other.cost)
        return +1;
      if (cost < other.cost)
        return -1;
      return 0;
    }
  }

  /**
   * Constructor that sets the map that the graph uses.
   *
   * @param map the map that the graph uses to map a data object to the node object it is stored in
   */
  public DijkstraGraph(MapADT<NodeType, Node> map) {
    super(map);
  }

  /**
   * This helper method creates a network of SearchNodes while computing the shortest path between
   * the provided start and end locations. The SearchNode that is returned by this method is
   * represents the end of the shortest path that is found: it's cost is the cost of that shortest
   * path, and the nodes linked together through predecessor references represent all of the nodes
   * along that shortest path (ordered from end to start).
   *
   * @param start the data item in the starting node for the path
   * @param end   the data item in the destination node for the path
   * @return SearchNode for the final end node within the shortest path
   * @throws NoSuchElementException when no path from start to end is found or when either start or
   *                                end data do not correspond to a graph node
   */
  protected SearchNode computeShortestPath(NodeType start, NodeType end)
      throws NoSuchElementException {
    Queue<SearchNode> priorityQueue = new PriorityQueue<>();

    MapADT<NodeType, SearchNode> nodeDistances = new PlaceholderMap<>();
    SearchNode one = new SearchNode(nodes.get(start), 0, null);
    priorityQueue.add(one);
    SearchNode endNode = null;

    while (!priorityQueue.isEmpty()) { //while loop for while the priority queue has nodes
      SearchNode current = priorityQueue.poll();
      if (current.node.data.equals(end)) { //if the current node is the end node
        endNode = current;
      }

      for (Edge edge : current.node.edgesLeaving) {
        Node successor = edge.successor; //successor node
        double cost = current.cost + edge.data.doubleValue();
        if (!nodeDistances.containsKey(successor.data) || cost < nodeDistances.get(
            successor.data).cost) {
          if (nodeDistances.containsKey(successor.data)) { //checking the PlaceholderMap
            SearchNode existingNode = nodeDistances.get(successor.data);
            existingNode.cost = cost;
            existingNode.predecessor = current;
          } else {
            SearchNode SuccessorSearchNode = new SearchNode(successor, cost, current);
            priorityQueue.add(SuccessorSearchNode);
            nodeDistances.put(successor.data, SuccessorSearchNode);
          }
        }

      }
    }
    if (endNode != null) {
      return endNode;
    } else {
      throw new NoSuchElementException("No path found");//exception thrown if there is no path found
    }
  }

  /**
   * Returns the list of data values from nodes along the shortest path from the node with the
   * provided start value through the node with the provided end value. This list of data values
   * starts with the start value, ends with the end value, and contains intermediary values in the
   * order they are encountered while traversing this shorteset path. This method uses Dijkstra's
   * shortest path algorithm to find this solution.
   *
   * @param start the data item in the starting node for the path
   * @param end   the data item in the destination node for the path
   * @return list of data item from node along this shortest path
   */
  public List<NodeType> shortestPathData(NodeType start, NodeType end) {
    SearchNode endNode = computeShortestPath(start, end);

    LinkedList<NodeType> path = new LinkedList<>();
    SearchNode current = endNode;
    while (current != null) {
      path.addFirst(current.node.data);
      current = current.predecessor;
    }
    // implement in step 5.4
    return path;
  }

  /**
   * Returns the cost of the path (sum over edge weights) of the shortest path freom the node
   * containing the start data to the node containing the end data. This method uses Dijkstra's
   * shortest path algorithm to find this solution.
   *
   * @param start the data item in the starting node for the path
   * @param end   the data item in the destination node for the path
   * @return the cost of the shortest path between these nodes
   */
  public double shortestPathCost(NodeType start, NodeType end) {

    // implement in step 5.4
    return computeShortestPath(start, end).cost;
  }

  /**
   * This tester tests the shortestPathData method with two graphs
   */
  @Test
  public void test1() {
    DijkstraGraph<String, Integer> graph = new DijkstraGraph<>(new PlaceholderMap<>());
    graph.insertNode("A");
    graph.insertNode("B");
    graph.insertEdge("A", "B", 4);
    graph.insertNode("C");
    graph.insertEdge("A", "C", 2);
    graph.insertNode("D");
    graph.insertEdge("C", "D", 5);
    graph.insertEdge("B", "D", 1);
    graph.insertNode("E");
    graph.insertEdge("B", "E", 10);
    graph.insertNode("F");
    graph.insertEdge("D", "F", 0);
    graph.insertEdge("F", "D", 2);
    graph.insertNode("H");
    graph.insertEdge("F", "H", 4);
    graph.insertNode("G");
    graph.insertEdge("G", "H", 4);
    graph.insertEdge("D", "E", 3);
    ArrayList<String> list1 = new ArrayList<>();
    list1.add("A");
    list1.add("B");
    list1.add("D");
    if (!graph.shortestPathData("A", "D").equals(list1)) {
      Assertions.fail("failed 1a");
    }
    List<String> list2 = new ArrayList<>();
    list2.add("A");
    list2.add("B");
    list2.add("D");
    list2.add("E");
    if (!graph.shortestPathData("A", "E").equals(list2)) {
      Assertions.fail("failed 1b " + graph.shortestPathData("A", "E"));
    }
  }

  /**
   * This tests shortestPathCost with two graphs
   */
  @Test
  public void test2() {
    DijkstraGraph<String, Integer> graph = new DijkstraGraph<>(new PlaceholderMap<>());
    graph.insertNode("A");
    graph.insertNode("B");
    graph.insertEdge("A", "B", 4);
    graph.insertNode("C");
    graph.insertEdge("A", "C", 2);
    graph.insertNode("D");
    graph.insertEdge("C", "D", 5);
    graph.insertEdge("B", "D", 1);
    graph.insertNode("E");
    graph.insertEdge("B", "E", 10);
    graph.insertNode("F");
    graph.insertEdge("D", "F", 0);
    graph.insertEdge("F", "D", 2);
    graph.insertNode("H");
    graph.insertEdge("F", "H", 4);
    graph.insertNode("G");
    graph.insertEdge("G", "H", 4);
    graph.insertEdge("D", "E", 3);
    if (graph.shortestPathCost("A", "D") != 5) {
      Assertions.fail("fail 2a");
    }
    if (graph.shortestPathCost("A", "E") != 8) {
      Assertions.fail("fail 2b");
    }
  }

  /**
   * This test tests the shortestPathCost and shortestPathData methods to see if they correctly
   * throw NoSuchElement exceptions
   */
  @Test
  public void test3() {
    DijkstraGraph<String, Integer> graph = new DijkstraGraph<>(new PlaceholderMap<>());
    graph.insertNode("A");
    graph.insertNode("B");
    graph.insertEdge("A", "B", 4);
    graph.insertNode("C");
    graph.insertEdge("A", "C", 2);
    graph.insertNode("D");
    graph.insertEdge("C", "D", 5);
    graph.insertEdge("B", "D", 1);
    graph.insertNode("E");
    graph.insertEdge("B", "E", 10);
    graph.insertNode("F");
    graph.insertEdge("D", "F", 0);
    graph.insertEdge("F", "D", 2);
    graph.insertNode("H");
    graph.insertEdge("F", "H", 4);
    graph.insertNode("G");
    graph.insertEdge("G", "H", 4);

    Assertions.assertThrows(NoSuchElementException.class, () -> graph.shortestPathData("A", "G"));


    Assertions.assertThrows(NoSuchElementException.class, () -> graph.shortestPathCost("A", "G"));


  }

}



