import java.util.List;

public class ShortestPath implements ShortestPathInterface {
  List<String> route;
  List<Integer> flightCosts;
  Integer totalCost;

  public ShortestPath(List<String> route, List<Integer> flightCosts, Integer totalCost) {
    this.route = route;
    this.flightCosts = flightCosts;
    this.totalCost = totalCost;
  }

  @Override
  public List<String> getRoute() {

    return this.route;
  }

  @Override
  public List<Integer> findMilesPerSegment() {
    return this.flightCosts;
  }

  @Override
  public int getTotalMiles() {
    return this.totalCost;
  }
}
