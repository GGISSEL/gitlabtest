import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import java.io.FileNotFoundException;
import java.util.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class BackendDeveloperTests {
  /**
   * Tests reading a small dot file that exists, passes if the actual number of nodes is the same as
   * the expected amount else fails
   */
  @Test
  public void testReadDataFromFile() {

    //Instantiate a Backend
    Backend backend = new Backend(new DijkstraGraph<String, Integer>(new PlaceholderMap<>()));
    //Import test dot data into the backend
    try {
      backend.readDataFromFile("flights.dot");
    } catch (FileNotFoundException e) {
      Assertions.fail("testReadDataFromFile- readDate file failed");
    }
    ShortestPathInterface path = backend.findShortestPath("ATL", "SFO");
    //System.out.println(path.getRoute() + " " + path.findMilesPerSegment() + " " + path.getTotalMiles());
    //System.out.println(backend.getSummary());
  }

  /**
   * Tests finding the shortest path of a path that exists, passes if the correct list of flights is
   * returned, else it fails
   */
  @Test
  public void testFindShortestPath() {
    //Instantiate a Backend
    Backend backend = new Backend(new DijkstraGraph<String, Integer>(new PlaceholderMap<>()));
    try {
      backend.readDataFromFile("smallFlights.dot");
    } catch (FileNotFoundException e) {
      Assertions.fail("testFindShortestPath- readDate file failed");
    }

    //Find the shortest path for a flight
    ShortestPathInterface shortestPath = backend.findShortestPath("ORD", "DTW");
    //The actual list for the shortest route
    List<String> result = shortestPath.getRoute();
    //The expected list for the shortest route
    List<String> expected = new ArrayList<>();
    //Adding the flights to the expected path
    expected.add("ORD");
    expected.add("RNO");
    expected.add("DTW");
    //System.out.println(result);
    //Checking if the expected is the same as the actual path
    assertEquals(expected, result);
  }

  /**
   * Tests getting the summary of the data file, passes if the expected summary is the same as the
   * actual summary returned, else fails
   */
  @Test
  public void testGetSummary() {
    Backend backend = new Backend(new DijkstraGraph<String, Integer>(new PlaceholderMap<>()));
    try {
      backend.readDataFromFile("smallFlights.dot");
    } catch (FileNotFoundException e) {
      Assertions.fail("testGetSummary- readDate file failed");
    }

    String expected = "5 airports, 6 flights, 9048 total miles";
    String actual = backend.getSummary();
    //System.out.println(actual);
    assertEquals(expected, actual);

  }

  /**
   * Tests trying to read a data file that does not exist and passes if a FileNotFoundException is
   * thrown, else fails
   */
  @Test
  public void testReadDataFromFileEmpty() {
    Backend backend = new Backend(new DijkstraGraph<String, Integer>(new PlaceholderMap<>()));
    try {
      backend.readDataFromFile("notReal.dot");
      Assertions.fail();
    } catch (FileNotFoundException e) {

    }

  }

  /**
   * Tests trying to find the shortest path of a path that does not exist, passes if a
   * FileNotFoundException is thrown, else it fails
   */
  @Test
  public void testPathDoesNotExist() {
    Backend backend = new Backend(new DijkstraGraph<String, Integer>(new PlaceholderMap<>()));

    try {
      backend.readDataFromFile("smallFlights.dot");
      backend.findShortestPath("DeathStar", "StarDestroyer");
      Assertions.fail();
    } catch (FileNotFoundException f) {
      Assertions.fail("testPathDoesNotExist- readDate file failed");
    } catch (NoSuchElementException e) {

    }
  }


  /**
   * Tests the miles per segment method, passes if the expected list is the same as the actual
   * returned list, else it fails
   */
  @Test
  public void testFindMilesPerSegment() {
    Backend backend = new Backend(new DijkstraGraph<String, Integer>(new PlaceholderMap<>()));
    try {
      backend.readDataFromFile("flights.dot");
    } catch (FileNotFoundException e) {
      Assertions.fail("testReadDataFromFile- readDate file failed");
    }
    ShortestPath path = (ShortestPath) backend.findShortestPath("SMF", "TPA");
    LinkedList<Integer> expected = new LinkedList<>();
    expected.add(1442);
    expected.add(917);
    LinkedList<Integer> actual = (LinkedList<Integer>) path.flightCosts;
    System.out.println(actual);
    //System.out.println(expected);
    assertEquals(expected, actual);
  }


  /**
   * Tests the shortestRoute method to see if it returns the correct miles for each flight in
   * the path
   */
  @Test
  public void integrationTest1() {
    TextUITester tester = new TextUITester("L\nflights.dot\nR\nATL\nSFO\nE");
    Backend backend = new Backend(new DijkstraGraph<String, Integer>(new PlaceholderMap<>()));
    FrontendInterface frontend = new Frontend(backend, new Scanner(System.in));
    frontend.mainMenu();
    String output = tester.checkOutput();
    if (!output.contains("Miles Between Airports: [2139]")) {
      Assertions.fail("Incorrect output");
    }

  }

  /**
   * Tests the showStats method to see if the correct stats are returned for the dot file
   */
  @Test
  public void integrationTest2() {
    TextUITester tester = new TextUITester("L\nflights.dot\nS\nE\n");
    Backend backend = new Backend(new DijkstraGraph<String, Integer>(new PlaceholderMap<>()));
    FrontendInterface frontend = new Frontend(backend, new Scanner(System.in));
    frontend.mainMenu();
    String output = tester.checkOutput();
    if (!output.contains("58 airports, 1598 flights, 2142457 total miles")) {
      Assertions.fail("Incorrect output");
    }
  }

}
