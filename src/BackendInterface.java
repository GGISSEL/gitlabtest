import java.io.FileNotFoundException;
import java.util.NoSuchElementException;

public interface BackendInterface{
    /**
    *Constructor
    *Parameters: GraphADT 
    *IndividualBackendInterface(GraphADT graph);
    */

     /**
         * Reads data from a specified file and populates the graph.
         * @param filePath The path to the data file.
         * @throws FileNotFoundException if that file does not exist
         */

    public void readDataFromFile(String filePath) throws FileNotFoundException;

    /**
         * Finds the shortest route from a start to a destination airport in the dataset.
         * @param start The starting airport.
         * @param end The destination airport.
         * @return The result of the shortest path search.
         */

    public ShortestPathInterface findShortestPath(String start, String end) throws NoSuchElementException;

    /**
         * Returns a string with statistics about the dataset.
         * @return Statistics about the dataset, including the number of nodes, the number of edges, and the total miles.
         */

    public String getSummary();





}
