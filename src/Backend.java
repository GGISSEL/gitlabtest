import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.LinkedList;

public class Backend implements BackendInterface {

  private GraphADT<String, Integer> graph;
  private int totalMiles = 0;

  public Backend(GraphADT<String, Integer> graph) {
    this.graph = graph;
  }

  /**
   * Reads the dot file containing the flights
   * @param filePath The path to the data file.
   * @throws FileNotFoundException when the path does not exist
   */
  @Override
  public void readDataFromFile(String filePath) throws FileNotFoundException {
    if (filePath == null) {
      throw new FileNotFoundException("file path null");
    }
    Scanner scnr = new Scanner(new File(filePath));
    scnr.nextLine(); //skip first line

    while (scnr.hasNext()) {

      String nextLine = scnr.nextLine();
      if (nextLine.contains("--")) {//checks if the line contains an edge
        String[] locations = nextLine.split("\"");//splits the line into segments containing the nodes and cost

        String startingLocation = locations[1];
        String destination = locations[3];
        locations[4] = locations[4].trim();
        String costString =
            locations[4].substring(locations[4].indexOf('=') + 1, locations[4].indexOf(']'));//separates the cost
        Integer cost = Integer.parseInt(costString);

        if (!graph.containsNode(startingLocation)) {//checks if the graph contains the starting location
          graph.insertNode(startingLocation);
        }
        if (!graph.containsNode(destination)) {//checks if the graph contains the destination
          graph.insertNode(destination);
        }
        graph.insertEdge(startingLocation, destination, cost);//inserts the flight into the graph
        graph.insertEdge(destination, startingLocation, cost);
        totalMiles = totalMiles + cost;

      }
    }

  }

  /**
   * Creates lists of the airports of the flight path, the miles for each flight and the
   * total miles traveled for the path
   * @param start The starting airport.
   * @param end The destination airport.
   * @return the shortestPath object
   * @throws NoSuchElementException
   */
  @Override
  public ShortestPathInterface findShortestPath(String start, String end)
      throws NoSuchElementException {
    List<String> pathList = graph.shortestPathData(start, end);
    Double totalCost = graph.shortestPathCost(start, end);

    List<String> route = graph.shortestPathData(start,end);//the list containing the flight route
    List<Integer> costs = new LinkedList<>();
    for (int i = 0; i < route.size() - 1; ++i) {
      costs.add((int) graph.shortestPathCost(route.get(i), route.get(i + 1)));//adds the miles of each flight to the costs list
    }
    Integer tCost = totalCost.intValue();//total miles in the path
    ShortestPath path = new ShortestPath(pathList, costs, tCost);

    return path;
  }

  /**
   * Creates a string summary of the dot file containing
   * the flights
   * @return a string of the total airports, total flights, and total miles
   */
  @Override
  public String getSummary() {
    String summary =
        graph.getNodeCount() + " airports, " + graph.getEdgeCount()/2 + " flights, " + totalMiles + " total miles";
    return summary;
  }
}
